import React from "react";
import Routes from "./Routes/index";
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './store/index';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';

require('dotenv').config()
function App() {
    return(
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <CssBaseline />
                <BrowserRouter>
                    <Switch>
                        <Route path="/" component={Routes} />
                    </Switch>
                </BrowserRouter>
            </PersistGate>
        </Provider>     
    )     
}

export default App;
