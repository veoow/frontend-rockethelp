import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';

import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import Can from '../Components/Can/can';
import { atualizarPermissoes } from '../permissoes/ability';

//Actions
import { Creators as ActionsUser } from '../store/ducks/Usuario';

import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import List from '@material-ui/core/List';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Badge from '@material-ui/core/Badge';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTachometerAlt, faListAlt, faBell, faUserAstronaut, faMeteor, faUserFriends, faCogs, faRobot, faIdBadge, faIdCardAlt } from '@fortawesome/free-solid-svg-icons';

import Typography from '@material-ui/core/Typography';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { Divider } from '@material-ui/core';

import Notifications from '../Components/Notifications/index';
import zIndex from '@material-ui/core/styles/zIndex';

const drawerWidth = 57;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    marginLeft: drawerWidth,
    background: '#6db65b',
    zIndex: '1201',
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },

  hide: {
    display: 'none',
  },
  
  drawer: {
    overflowX: 'hidden',
    width: drawerWidth,
    backgroundColor: 'ghostWhite',
    height: '100%',
    paddingTop: '67px',
    zIndex: '990'
  },

  drawerContent: {
    display: 'flex',
    justifyContent: 'space-between',
    zIndex: '990'
  },

  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,

    '&h6':{
      marginLeft: '100px',
    }
  },

  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
});

class HeaderNav extends Component {
    state = {
      open: false,
      notification: false,
      menu: [
        {
          text: "Dashboard",
          name: 'dashboard',
          link: "/app/dashboard",
          icon: faTachometerAlt
        },
        {
            text: "Tickets",
            name: 'tickets',
            link: "/app/tickets",
            icon: faListAlt
        },
        {
          text: 'Usuarios',
          link: '/app/usuarios',
          name: 'usuarios',
          icon: faIdCardAlt
        },
        {
          text: 'Configurações',
          link: '/app/chamados',
          icon: faCogs  
        },
        {
          text: 'ChatBOT',
          link: '/app/chatbot',
          icon: faRobot
        }
      ],
      invisible: false,
      count: 10,
      redirect: false,
    }

    UNSAFE_componentWillMount() {
      const { user } = this.props;
      atualizarPermissoes(user)
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
      const { user } = nextProps
      atualizarPermissoes(user)
    }

    handleToggle = () => {
      this.setState(state => ({ open: !state.open }));
    };
  
    handleClose = event => {
      if (this.anchorEl.contains(event.target)) {
        return;
      }
  
      this.setState({ open: false });
    };

    redirect = () => {
      return <Redirect to="/" />
    }

    handleLogout = async () => {
      const { SetLogout } = this.props;
      SetLogout();
      localStorage.removeItem('token')
    }

    render(){
        const { classes, theme, user } = this.props;
        const { menu, invisible, count, open } = this.state;
        
        return(
            <div>
                <AppBar
                    position="fixed"
                    className={ classes.appBar }
                    elevation0                    
                >
                  <div className={classes.drawerContent}>
                    <Toolbar style={{ padding: '0px' }}>
                      <ListItem style={{ background: '#406b36', height: '100%', width: '58px', justifyContent: 'center' }}>
                        <FontAwesomeIcon style={{color:"white"}} className='fa-2x' icon={faMeteor} />
                      </ListItem>
                        <Typography style={{ fontFamily: 'raleway', fontSize: '25px', fontWeight: '300', color: "white", margin: '0px 15px' }} variant="h6" noWrap>
                            RocketHelp
                        </Typography>
                    </Toolbar>
                    <div style={{ margin: '10px 50px', display: 'flex' }}>                      
                      <Notifications />

                      <IconButton 
                        onClick={this.handleToggle}
                        buttonRef={node => {
                          this.anchorEl = node;
                        }}
                      >
                        <FontAwesomeIcon style={{color:"white"}} className='fa-sm' icon={faUserAstronaut} />
                      
                        <Popper open={open} anchorEl={this.anchorEl} transition disablePortal>
                          {({ TransitionProps, placement }) => (
                            <Grow
                              {...TransitionProps}
                              id="menu-list-grow"
                              style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                            >
                              <Paper>
                                <p style={{ padding: '15px 5px', fontSize: '17px', margin: '0px', color: '#6db65b' }}>{ user.username }</p>
                                <Divider/>
                                <ClickAwayListener onClickAway={this.handleClose}>
                                  <MenuList>                                    
                                    <MenuItem onClick={this.handleClose}>My account</MenuItem>                                    
                                    <MenuItem onClick={this.handleLogout}>Sair</MenuItem>
                                  </MenuList>
                                </ClickAwayListener>
                              </Paper>
                            </Grow>
                          )}
                        </Popper>
                      </IconButton>
                    </div>                    
                  </div>
                </AppBar>

                <Drawer
                  variant="permanent"
                  elevation={16}
                >
                  <List className={classes.drawer}>
                    <div>
                      {menu.map((item, index) => (
                        <Can do="read" on={{ __type: ''+ item.name +'' }}>
                          <Tooltip title={item.text} placement="right">
                            <Link to={ item.link }>
                              <ListItem button key={item}>                          
                                <ListItemIcon>
                                  <FontAwesomeIcon style={{color:"#6DB65B"}} className='fa-lg' icon={item.icon} />
                                </ListItemIcon>
                              </ListItem>
                            </Link>
                          </Tooltip>
                        </Can>
                      ))}
                    </div>
                  </List>
                </Drawer>
            </div>
        );
    }
}

HeaderNav.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ usuario }) => {
  const { user_info } = usuario;
  
  return {
      user: user_info.user
  }
}

const { SetLogout } = ActionsUser;

export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, {
    SetLogout
  })
)(HeaderNav);