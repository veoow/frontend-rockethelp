import { create } from 'apisauce';
let token = localStorage.getItem('token');

const api = create({
    baseURL: process.env.REACT_APP_API_URL ? process.env.REACT_APP_API_URL : '',
    //baseURL: 'http://localhost:3001'
})
api.setHeader('Authorization', 'Bearer ' + token)

export default api