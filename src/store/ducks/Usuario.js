import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
    Usuario: [ 'payload' ],
    SetLogado: [ 'payload' ],
    SetLogout: [ 'payload' ]
})

const INITIAL_STATE = {
    user_info: [],
    logado: false,
};

const SetLogado = (state = INITIAL_STATE, action) => ({
    ...state,
    logado: action.payload
})

const SetLogout = (state = INITIAL_STATE, action) => ({
    ...state,
    logado: false,
    user_info: []
})

const Usuario = (state = INITIAL_STATE, action) => {
    const user = action.payload;
    return {
        ...state,
        user_info: user
    }
}

export const HANDLERS = {    
    [Types.USUARIO]: Usuario,
    [Types.SET_LOGOUT]: SetLogout,
    [Types.SET_LOGADO]: SetLogado
}

export default createReducer(INITIAL_STATE, HANDLERS)