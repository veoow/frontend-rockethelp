import { combineReducers } from "redux";

import usuario from './Usuario';

export default combineReducers({
    usuario
})