import { AbilityBuilder } from '@casl/ability';

// Check if subject name is "Project"
function subjectName(item) {
  if (!item || typeof item === "string") {
    return item;
  }

  return item.__type;
}

export const ability = AbilityBuilder.define({ subjectName }, (can) => {
  can(["read"], 'dashboard');
  can(["read"], 'tickets');
  can(["read"], 'clientes');
});

export const atualizarPermissoes = (user) => {
    ability.update(defineRulesFor(user))
}

const defineRulesFor = (user) => {
    const { can, rules } = AbilityBuilder.extract();
    if(!! user){
        if(user.hasOwnProperty('modulos')){
            for(let key in user.modulos){
                if(user.modulos[key]){                                    
                    for(let permissao in user.modulos[key]){
                        can(permissao, key)
                    }
                }
            }
        }
    }
    return rules
}