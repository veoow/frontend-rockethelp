import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';
import io from 'socket.io-client';
import api from '../../api/api';

import Header from '../../Header/index'
import Usuarios from '../../Components/Usuarios/index';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

import UserForm from '../../Components/Usuarios/Cadastrar/index';

const styles = theme => ({
    container: {
        width: '100%',
        padding: '24px',
        flexGrow: '1',
        height: '100%',
    },

    margin: {
        margin: theme.spacing.unit,
    },
})

class UsersPage extends Component {
    state = {
        data: [],
        loading: false,
        usuario: {
            cliente: false,
            usuario: true
        }
    }

    componentDidMount() {
        const socket = io(process.env.REACT_APP_API_URL)
        const { user } = this.props;
        api.get('/clients', { 
            companyID: user.companyID,
            userType: user.userType
        }).then((res) => {
            let data = res.data;
            let newData = [];
            
            data.map((item) => {
                newData.push({ name: item.username, createdAt: item.createdAt, userType: item.userType })
            })

            this.setState({
                data: newData
            })
        })
    }

    render(){
        const { classes, match, user } = this.props;
        const { loading, data, usuario } = this.state;

        return(
            <div style={{ margin: '73px' }}>
                <Header />
                <div className={ classes.container }>
                    <Paper style={{ display: 'flex', justifyContent: 'space-between', height: '50px' }} >
                        <Typography style={{ padding: '10px 15px', color: '#81c784', fontSize: '20px' }}>
                            Usuarios
                        </Typography>
                        <UserForm usuario={usuario}/>
                    </Paper>
                    <br />
                    <div>
                        <Usuarios loading={loading} data={data} usuario={usuario} />                        
                    </div>
                </div>
            </div>
        );
    }
}

UsersPage.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user: user_info.user
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, {})
)(UsersPage);