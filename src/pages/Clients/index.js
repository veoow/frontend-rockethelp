import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Header from '../../Header/index'
import ClientsComp from '../../Components/Clients/index';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

import CreateNew from '../../Components/Usuarios/Cadastrar/index'

const styles = theme => ({
    container: {
        width: '100%',
        padding: '24px',
        flexGrow: '1',
        height: '100%',
    },

    margin: {
        margin: theme.spacing.unit,
    },
})

class ClientsPage extends Component {
    state = {
        usuario: {
            cliente: true,
            usuario: false
        }
    }

    render(){
        const { classes, match } = this.props;
        const { usuario } = this.state;
        return(
            <div style={{ margin: '73px' }}>
                <Header />
                <div className={ classes.container }>
                    <Paper style={{ display: 'flex', backgroundColor: 'white', justifyContent: 'space-between', height: '50px' }} >
                        <Typography style={{ padding: '15px', color: '#81c784', fontSize: '20px' }}>
                            Clientes
                        </Typography>
                        <CreateNew usuario={usuario} />
                    </Paper>
                    <br />
                    <div>
                        <ClientsComp />                        
                    </div>
                </div>
            </div>
        );
    }
}

ClientsPage.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles, { withTheme: true })(ClientsPage);