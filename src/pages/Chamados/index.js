import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Header from '../../Header/index'
import ChamadosComp from '../../Components/Chamados/index';
import CreateNew from '../../Components/Chamados/createCall/index';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

const styles = theme => ({
    container: {
        width: '100%',
        padding: '24px',
        flexGrow: '1',
        height: '100%',
    },

    margin: {
        margin: theme.spacing.unit,
    },    
})

class Chamados extends Component {

    state = {
        open: false,
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    }

    handleClose = () => {
        this.setState({ open: false });
    }

    render(){
        const { open } = this.state;
        const { classes, match } = this.props;

        return(
            <div style={{ margin: '73px' }}>
                <Header />
                <div className={ classes.container }>
                    <Paper style={{ display: 'flex', justifyContent: 'space-between', height: '50px' }} >
                        <Typography style={{ padding: '10px 15px', color: '#81c784', fontSize: '20px' }}>
                            Tickets
                        </Typography>
                        <CreateNew open={open} handleClickOpen={this.handleClickOpen} handleClose={this.handleClose} />
                    </Paper>
                    <br />
                    <div>
                        <ChamadosComp />                        
                    </div>
                </div>
            </div>
        );
    }
}

Chamados.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles, { withTheme: true })(Chamados);