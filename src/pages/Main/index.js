import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Header from '../../Header/index'
import DashBoard from '../../Components/Dash/index';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

const styles = theme => ({
    container: {
        width: '100%',
        padding: '24px',
        flexGrow: '1',
        height: '100%',
    },

    margin: {
        margin: theme.spacing.unit,
    },
})

class Dash extends Component {
    render(){
        const { classes, match } = this.props;

        return(
            <div style={{ margin: '73px' }}>                
                <Header />
                <div className={ classes.container }>
                    <Paper style={{ display: 'flex', backgroundColor: 'white', justifyContent: 'space-between', height: '50px' }} >
                        <Typography style={{ padding: '10px 15px', color: '#81c784', fontSize: '20px' }}>
                            DashBoard
                        </Typography>                        
                    </Paper>
                    <br />
                    <div>
                        <DashBoard />                        
                    </div>
                </div>
            </div>
        );
    }
}

Dash.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles, { withTheme: true })(Dash);