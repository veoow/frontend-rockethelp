import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';

//Ducks Actions
import { Creators as ActionsUser } from '../../store/ducks/Usuario';

import Paper from '@material-ui/core/Paper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMeteor, faUser, faKey } from '@fortawesome/free-solid-svg-icons';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

import auth from '../../auth/auth';
import api from '../../api/api';

const styles = theme => ({
    body: {
         margin: '0'
    },
      
    bg: {
        animation: 'slide 3s ease-in-out infinite alternate',
        backgroundImage: 'linear-gradient(-60deg, #6c3 50%, #238013 50%)',
        bottom:'0',
        left:'-50%',
        opacity: '.5',
        position: 'fixed',
        right: '-50%',
        top:0,
        zIndex: '-1',
    },
      
    bg2: {
        animationDirection: 'alternate-reverse',
        animationDuration: '8s',
    },
    
    bg3: {
        animationDuration: '10s',
    },
    
    content: {
        padding: '14px',
        textAlign: 'center',
        width: '18%',
        display: 'flex',
        borderRadius: '0px'
    },

    welcomePaper: {
        width: '28%',
        borderRadius: '0px',
        background: 'teal',
        color: 'white',
        fontFamily: 'karla',
        textAlign: 'center',        
    },

    formControl: {
        margin: theme.spacing.unit,
    },

    buttonProgress: {
        color: 'teal',
        position: 'absolute',
        top: '64%',
        left: '64%',
        marginTop: -12,
        marginLeft: -12,
    },

    '@keyframes slide': {
        '0%': {
          transform: 'translateX(-25%)',
        },
        '100%': {
          transform: 'translateX(25%)',
        }
    }
      
})

class login extends Component {
    state = {
        logged: false,
        error: '',
        username: '',
        password: '',
        loading: false
    }

    signIn =  async (username, password) => {
        this.setState({ loading: true });
        await auth.login(username, password)
        await this.props.Usuario({ user: auth.user });
        console.log(auth.user.username);
        auth.user.username && auth.authenticated == true ?  window.location.reload() : console.log('Token no provided');
        this.setState({ loading: false });
    }

    onUserNameChange = (event) => {
        this.setState({
            username: event.target.value 
        })
    }

    onPasswordChange = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    render(){
        const { classes } = this.props;
        const { username, password, error, loading } = this.state;

        return(
            <div>
                <div className={classes.bg}></div>
                <div className={`${classes.bg} ${classes.bg2}`}></div>
                <div class={`${classes.bg} ${classes.bg3}`}></div>

                <div style={{ textAlign: 'center', marginTop: '150px' }}>
                    <FontAwesomeIcon style={{color:"white"}} className='fa-5x' icon={faMeteor} />
                    <h1 style={{ fontFamily: 'raleway', fontSize: '50px', fontWeight: '300', color: 'white' }}>RocketHelp</h1>
                </div>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <Paper className={ classes.welcomePaper }>
                        <div style={{ margin: '100px' }}>
                            <h1>Bem-Vindo</h1>
                            <p>Esperamos ajudar você a ajudar com eficiencia</p>
                        </div>
                    </Paper>
                    <Paper className={classes.content}>
                        <div>
                            <div>
                                <FormControl className={classes.formControl}>
                                    <TextField
                                        id="outlined-username"
                                        label="Username"
                                        className={classes.textField}
                                        onChange={this.onUserNameChange}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                    { error == 'Usuario não encontrado' ? <FormHelperText id="component-error-text" style={{ color: 'red' }}>Ops.. não encontramos este username</FormHelperText> : ''}
                                </FormControl>
                                <FormControl className={classes.formControl}>
                                    <TextField
                                        id="outlined-password"
                                        label="Password"
                                        className={classes.textField}
                                        onChange={this.onPasswordChange}
                                        type="password"
                                        margin="normal"
                                        variant="outlined"
                                    />
                                { error == 'Invalid password' ? <FormHelperText id="component-error-text" style={{ color: 'red' }}>Senha Inválida, tente novamente</FormHelperText> : ''}
                                </FormControl>
                            </div>
                            <br />
                            <div style={{ textAlign: 'right', margin: 'auto', width: '60%' }}>
                                <Button variant="contained" color="primary" style={{ color: 'white', width: '100%' }} disabled={ loading } onClick={this.signIn.bind(this, username, password)} className={classes.button}>
                                    Entrar
                                </Button>
                                {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
                            </div>
                        </div>
                    </Paper>                    
                </div>
            </div>
        );
    }
}

login.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user_info
    }
}

const { Usuario } = ActionsUser;

export default compose(
    withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, {
        Usuario
    })
)(login);