import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';
import api from '../../../api/api';

import Tooltip from '@material-ui/core/Tooltip';
import TextField from '@material-ui/core/TextField';

import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

const styles = theme => ({
    textField: {
        margin: '5px',
        marginBottom: '10px',
        width: 174,
      },
})

class FormCreate extends Component {
    state = {
        values: [],
        response: []
    }

    handleChange = name => event => {
        const { values } = this.state;
        this.setState({ values: {...values, [name]: event.target.value} });        
    }

    handleSubmit = () => {
        const { response, values, uploadedFiles } = this.state;
        const { user, usuario } = this.props;
        
        this.setState({
            response: {
                username: values.username,
                password: values.password,
                email: values.email,
                telefone: values.telefone,
                userType: usuario.cliente ? 'cliente' : 'employee',
                cliente: usuario.cliente ? 'cliente' : '',
                companyID: user.companyID
            }
        })

        api.post('/register', {
            username: response.username,
            password: response.password,
            email: response.email,
            telefone: response.telefone,
            userType: response.userType,
            cliente: response.cliente,
            companyID: response.companyID,
            pacote: 'free'
        })
    }

    render(){
        const { classes, onUpload } = this.props;
        const { values, response, uploadedFiles } = this.state;

        return(
            <div>
               <form className={`${classes.container} d-flex flex-wrap`} noValidate autoComplete="off">
                    <TextField
                        id="standard-name"
                        label="Nome"
                        className={classes.textField}
                        onChange={this.handleChange('username')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="Senha"
                        className={classes.textField}
                        onChange={this.handleChange('password')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="Confirmar Senha"
                        className={classes.textField}
                        onChange={this.handleChange('senhaConfirm')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="email"
                        className={classes.textField}
                        onChange={this.handleChange('email')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="Telefone"
                        className={classes.textField}
                        onChange={this.handleChange('telefone')}
                        margin="normal"
                    />
                    <br />

                    <Divider />
                    <div style={{ textAlign: 'end', margin: '15px 0px' }}>
                        <Button onClick={ this.handleClose } color="secondary">
                            Cancelar
                        </Button>
                        <Button variant="contained" style={{ color: 'white' }} onClick={ this.handleSubmit } color="primary">
                            Criar Novo
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user: user_info.user
    }
}

export default compose(
    withStyles(styles),
    connect(mapStateToProps, {})
)(FormCreate)