import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import api from '../../../api/api';

import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Divider from '@material-ui/core/Divider';

import Form from './form';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faTrash, faWindowClose, faUserPlus } from '@fortawesome/free-solid-svg-icons';

const styles = theme => ({
    margin: {
        margin: theme.spacing.unit,
    },

    dialog: {
        width: '600px',
        height: '50%'
    },

    dialogTitle: {
        backgroundColor: '#81c784',
        padding: '5px 5px',
        
        '& h2':{
            color: '#00695c',
            justifyContent: 'space-between',
            display: 'flex'
        }
    },

    createNew: {
        margin: '5px'
    }
})

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class Usuario extends Component {

    state = {
        dados: [],
        open: false,
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    }

    handleClose = () => {
        this.setState({ open: false });
    }

    handleVerMais = (id) =>{        
        api.get('http://localhost:3001/'+ id)
        .then(res => {
            this.setState({
                dados: res.data,
                open: true
            })
        })
    }

    handleCreate = (response) => {
        console.log(response)
    }

    render(){
        const { classes,  id, handleSubmit, usuario } = this.props;
        const { dados, open } = this.state;
        return(
            <div>
                <Tooltip title="Criar novo Usuario" placement="bottom"> 
                    <IconButton color="secondary" className={ classes.createNew } onClick={this.handleClickOpen}>
                        <FontAwesomeIcon style={{ color:'#81c784' }} className='fa-xs' icon={faUserPlus} />
                    </IconButton>
                </Tooltip>                
               
                <Dialog
                    open={open}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"                    
                >   
                    <div className={ classes.dialog }>
                        <DialogTitle id="alert-dialog-slide-title" className={ classes.dialogTitle }>
                            <div style={{ margin: '20px', color: 'white' }}>
                                Cadastrar novo Usuario
                            </div>
                            <div>  
                                <IconButton className={ classes.margin } onClick={this.handleClose}>
                                    <FontAwesomeIcon style={{ color:'white' }} className="fa-sm" icon={ faWindowClose } />
                                </IconButton>
                            </div>
                        </DialogTitle>
                        <br />
                        <DialogContent style={{ padding: '0 24px 0px' }}>
                            <Form usuario={usuario}/>
                        </DialogContent>
                    </div>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(Usuario)