import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';
import moment from 'moment';

import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';

import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import LinearProgress from '@material-ui/core/LinearProgress';

import UserForm from './Cadastrar/index';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretSquareDown, faUser } from '@fortawesome/free-solid-svg-icons';

moment.locale('pt-br');

const styles = theme => ({
    margin: {
        margin: 0,
    },

    chip: {
        color: 'white'
    },

    card: {
        maxWidth: '100%',
        minWidth: '100%',
        margin: '0px 10px 10px 0px'
    },

    emptyUser: {
        padding: '10px',
        margin: 'auto 39%',
        textAlign: 'center',
        backgroundColor: 'ghostwhite',
        color: '#6db65b'
    }
})

class UsuariosComponent extends Component {    

    render(){
        const { classes, user, loading, data, usuario } = this.props;
        
        return(
            <div>
                { loading ? (<LinearProgress color="secondary" />) : ''}

                { data.length > 0 ? (
                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                        {
                            data.map((item) => {
                                return(                            
                                    <Card className={classes.card}>
                                        <CardHeader
                                            avatar={
                                                <Avatar style={{ color: 'white' }}>{item.name.charAt(0).toUpperCase()}</Avatar>
                                            }
                                            action={
                                                <IconButton color="primary" aria-label="Settings">
                                                    <FontAwesomeIcon size="sm" icon={faCaretSquareDown} />
                                                </IconButton>
                                            }
                                            title={
                                                `${item.name} - ${item.userType}`
                                            }
                                            subheader={moment(item.createdAt).format('LLL')}
                                        />
                                    </Card>
                                )
                            })
                        }
                    </div>
                    ):(
                        <div className={`${classes.emptyUser} d-flex`}>
                            <Typography variant="h6" component="h6">Você não possui usuarios registrados, registre o primeiro</Typography>
                            <div className={ classes.createFirst }>
                                    <UserForm usuario={usuario}/>
                            </div>
                        </div>
                    )
                }
            </div>
        );
    }
}

UsuariosComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user: user_info.user
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, {})
)(UsuariosComponent);