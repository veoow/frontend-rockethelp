import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';
import api from '../../../api/api';

import Tooltip from '@material-ui/core/Tooltip';
import TextField from '@material-ui/core/TextField';

import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {} from '@fortawesome/free-solid-svg-icons';

import Dropzone from 'react-dropzone';
import { DropContainer, UploadMessage } from './style';
import filesize from 'filesize';
import { uniqueId } from 'loadsh'

const styles = theme => ({
    textField: {
        margin: '5px',
        marginBottom: '10px',
        width: 174,
      },
})

class FormCreate extends Component {
    state = {
        uploadedFiles: '',
        values: [],
        response: []
    }

    renderDragMessage = (isDragActive, isDragReject) => {
        if (!isDragActive) {
            return <UploadMessage>Arraste sua imagem aqui...</UploadMessage>;
        }

        if (isDragReject) {
            return <UploadMessage type="error">Arquivo não suportado</UploadMessage>;
        }

        return <UploadMessage type="success">Solte sua imagem aqui</UploadMessage>;
    };

    handleUpload = files => {
        const uploadedFiles = files.map(file => ({
            file,
            id: uniqueId(),
            name: file.name,
            readableSize: filesize(file.size),
            preview: URL.createObjectURL(file),
            progress: 0,
            uploaded: false,
            error: false,
            url: URL.createObjectURL(file)
        }));
        
        this.setState({
          uploadedFiles
        });
    };

    handleChange = name => event => {
        const { values } = this.state;
        this.setState({ values: {...values, [name]: event.target.value} });        
    }

    updateFile = (id, data) => {
        this.setState({
            uploadedFiles: this.state.uploadedFiles.map(uploadedFile => {
                return id === uploadedFile.id
                ? { ...uploadedFile, ...data }
                : uploadedFile;
            })
        });
    };

    handleSubmit = async files => {
        const { response, values, uploadedFiles } = this.state;
        const { user_info } = this.props;
        
        let data = new FormData();
        let user = user_info.user;

        let answer = {
            title: values.title,
            problem: values.problem,
            priority: values.priority,
            status: values.status,
            uploadedFiles
        }

        uploadedFiles.length > 0 ? data.append("file", uploadedFiles[0].file, uploadedFiles[0].name, uploadedFiles[0].url) : data = null

        await api.post('/', {
            title: answer.title,
            description: answer.problem,
            companyID: user.companyID,
            company: user.username,
            clientID: user._id,
            priority: answer.priority,
            status: answer.status,
        }).then((res) => {
            res.ok ? 
                data !== null ?
                    api.post('/upload/' + res.data._id , data) : console.log('Data is null')
            : console.log('Response is false')
        })
    }

    render(){
        const { classes, onUpload } = this.props;
        const { values, response, uploadedFiles } = this.state;
        return(
            <div>
               <form className={`${classes.container} d-flex flex-wrap`} noValidate autoComplete="off">
                    <TextField
                        id="standard-name"
                        label="Title"
                        className={classes.textField}
                        onChange={this.handleChange('title')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="Priority"
                        className={classes.textField}
                        onChange={this.handleChange('priority')}
                        margin="normal"
                    />
                    <TextField
                        id="standard-name"
                        label="Status"
                        className={classes.textField}
                        onChange={this.handleChange('status')}
                        margin="normal"
                    />
                    <TextField
                        id="outlined-multiline-flexible"
                        label="Problema..."
                        variant="outlined"
                        multiline
                        rowsMax="5"
                        onChange={this.handleChange('problem')}
                        fullWidth
                        margin="normal"
                    />
                    <br />
                    <Dropzone accept="image/*" onDropAccepted={this.handleUpload} >
                        {
                            ({ getRootProps, getInputProps, isDragActive, isDragReject }) => (
                                <DropContainer {...getRootProps()} isDragActive={isDragActive} isDragReject={isDragReject}>
                                    <input {...getInputProps()} />
                                    {this.renderDragMessage(isDragActive, isDragReject)}
                                </DropContainer>
                            )
                        }
                    </Dropzone>
                    <img src={ uploadedFiles ? uploadedFiles[0].preview : '' } style={{ width: '100%', margin: '10px 0px' }}/>

                    <Divider />
                    <div style={{ textAlign: 'end', margin: '15px 0px' }}>
                        <Button onClick={ this.handleClose } color="secondary">
                            Cancelar
                        </Button>
                        <Button variant="contained" style={{ color: 'white' }} onClick={ this.handleSubmit } color="primary">
                            Criar Novo
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user_info
    }
}

export default compose(
    withStyles(styles),
    connect(mapStateToProps, {})
)(FormCreate)