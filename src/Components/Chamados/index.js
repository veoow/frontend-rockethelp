import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';
import io from 'socket.io-client';
import api from '../../api/api';
import moment from 'moment';

import OpenCall from './openCall/index';

import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import LinearProgress from '@material-ui/core/LinearProgress';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faExpand, faChevronDown, faHeadset, faUser, faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';

const styles = theme => ({
    margin: {
        margin: 0,
    },

    chip: {
        color: 'white'
    }
})

class ChamadosComponent extends Component {
    state = {
        columns: [
            'Data', 'Title', 'Created By', 'Status', 'Priority', 'Avaliação', 'Responsavel', 'Ver +'
        ],

        dataReceive: [],
        image: [],

        options: { 
            filterType: "dropdown",
            responsive: "scroll",
            selectableRows: 'none',
            textLabels: {
                body: {
                  noMatch: "Sem dados a exibir!",
                  toolTip: "Sort",
                }
            }
        },

        anchorEl: null,
        id: '',
        status: '',
        
        loading: false
    }

    componentDidMount() {
        const socket = io(process.env.REACT_APP_API_URL)
        const { dataReceive } = this.state;
        const { user_info } = this.props;
        
        let oldData = dataReceive;
        let user = user_info.user;

        this.setState({
            loading: true
        })

        api.get('/', {
            userType: user.userType,
            companyID: user.companyID,
            clientID: user._id
        }).then(res => this.setState({
                dataReceive: res.data,
                loading: false
            }))

        socket.on('NewCall', data => {
            api.get('/', {
                userType: user.userType,
                companyID: user.companyID,
                clientID: user._id
            }).then(res => this.setState({
                dataReceive: res.data,
                loading: false
            }))          
        })

        socket.on('ItemUpdate', data => {
            api.get('/', {
                userType: user.userType,
                companyID: user.companyID,
                clientID: user._id
            }).then(res => this.setState({
                dataReceive: res.data
            }))          
        })
    }

    handleClick = (id, event) => {
        this.setState({ anchorEl: event.currentTarget, id: id });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    updateStatus = (value) => {
        const { id } = this.state;
        const { user_info } = this.props;

        api.put('/' + id.id, {
            status: value
        })
        .then(res => console.log(res))

        this.handleClose();
    }

    getCall = (id) => {
        const { user_info } = this.props;
        let response = user_info.user.username
        api.put('/' + id, {
            response: response
        })
        .then(res => console.log(res))
    }

    render(){
        const { classes, user_info } = this.props;
        const { columns, dataReceive, options, anchorEl, input, loading, image } = this.state;
        let user = user_info.user;

        return(
            <div>
                { loading ? (<LinearProgress color="secondary" />) : ''}
                <MuiThemeProvider>
                    <MUIDataTable
                        data={dataReceive !== '' ? dataReceive.map(item => {               
                            return(
                                [
                                    item.createdAt ? moment(item.createdAt).format('LLL') : '',                                                 
                                    item.title ? item.title : '',
                                    item.company ? item.company : '',
                                    item.status  ? 
                                        user.userType != 'cliente' ? <Chip label={item.status} color="secondary" clickable onDelete={this.handleClick.bind(this, { id: item._id})} deleteIcon={<FontAwesomeIcon style={{ color:'white', padding: '5px' }} className='fa-lg' icon={faChevronDown} />} className={classes.chip} /> 
                                        : <Chip label={item.status} color="secondary"  className={classes.chip} />
                                    : '',
                                    item.priority ? item.priority : 'Easy',
                                    item.rating ? <Chip label={item.rating + ' Estrelas'} avatar={<Avatar style={{ color: 'yellow' }}><FontAwesomeIcon className='fa-sm' icon={faStarHalfAlt} /></Avatar>} color="secondary" className={classes.chip} /> : 'Ainda não terminado',
                                    user.userType != 'cliente' ? 
                                        item.response ? 
                                            <Chip label={item.response} avatar={<Avatar style={{ color: 'white' }}><FontAwesomeIcon className='fa-sm' icon={faUser} /></Avatar>} color="secondary" className={classes.chip} /> 
                                            : <Chip label={'Assumir Chamado'} color="secondary" clickable onDelete={this.getCall.bind(this, item._id)} deleteIcon={<FontAwesomeIcon style={{ color:'white', padding: '5px', fontSize: '25px' }} className='fa-lg' icon={faHeadset} />} />
                                        : item.response ? 
                                        <Chip label={item.response} avatar={<Avatar style={{ color: 'white' }}><FontAwesomeIcon className='fa-sm' icon={faUser} /></Avatar>} color="secondary" className={classes.chip} />
                                            : <Chip label={'Vazio'} />,                                                       
                                    item.vermais ? item.vermais : (<OpenCall id={item._id}/>)
                                ]
                            )
                        }): ''}                        
                        columns={columns}
                        options={options}
                    />
                </MuiThemeProvider>  

                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}                    
                >
                    <MenuItem onClick={this.updateStatus.bind(this, 'Todo')}>Todo</MenuItem>
                    <MenuItem onClick={this.updateStatus.bind(this, 'InProgress')}>InProgress</MenuItem>
                    <MenuItem onClick={this.updateStatus.bind(this, 'Done')}>Done</MenuItem>
                </Menu>           
            </div>
        );
    }
}

ChamadosComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user_info
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, {})
)(ChamadosComponent);