import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';
import io from 'socket.io-client'
import api from '../../../../api/api';

import ScrollToBottom from 'react-scroll-to-bottom';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane, faUser } from '@fortawesome/free-solid-svg-icons';

import { ImpulseSpinner } from 'react-spinners-kit';

const styles = theme => ({
    container: {
        fontFamily: 'Raleway'
    },

    chip: {
        color: 'white',
    },

    scroll: {
        height: 300,
    
        '& ::-webkit-scrollbar-track': {
            backgroundColor: '#F4F4F4',
            borderRadius: '10px',
        },
        '& ::-webkit-scrollbar': {
            width: '6px',
            borderRadius: '10px',
            background: '#F4F4F4'
        },
        '& ::-webkit-scrollbar-thumb': {
            background: '#dad7d7',
            borderRadius: '10px',
        }
    },
})

class Chat extends Component {

    state = {
        newMessage: '',
        mensagens: [],
    }

    onKeyDown = (event) => {
        if (event.key === 'Enter') {
          event.preventDefault();
          this.sendMenssage();
        }
    }

    handleChange = (event) => {
        this.setState({
            newMessage: event.target.value
        })
    }

    sendMenssage = async () => {
        const { newMessage } = this.state;
        const { user_info, id, mensagens } = this.props;
        let user = user_info.user;

        newMessage ?
            await api.put('/' + id, {
                messages: [
                    ...mensagens,
                    { username: user.username, message: newMessage }
                ]
            }).then(this.setState({ newMessage: '' }))
        : console.log('Mensagem Vazia')
    }

    render(){
        const { classes, user_info, mensagens, loading } = this.props;
        let user = user_info.user;
        return(
            <div className={ classes.container }>
                <ScrollToBottom animating className={ classes.scroll }>                                        
                    {  mensagens ? mensagens.map(message => {
                            if(message.username == user.username){
                                return (
                                    <Slide direction="up" in={true} mountOnEnter unmountOnExit>
                                        <div>                                        
                                            <Chip label={message.username} avatar={<Avatar style={{ color: 'white' }}>{message.username.charAt(0).toUpperCase()}</Avatar>} color="secondary" className={classes.chip} />
                                            <p style={{ padding: '0px 20px', marginTop: '5px', marginBottom: '15px' }}> { message.message } </p>                                        
                                        </div>
                                    </Slide>
                                )
                            }else{
                                return (
                                    <div style={{ textAlign: 'right' }}>
                                        <Chip label={message.username} avatar={<Avatar style={{ color: 'white' }}>{message.username.charAt(0).toUpperCase()}</Avatar>} color="secondary" className={classes.chip} />
                                        <p style={{ padding: '0px 20px', marginTop: '5px', marginBottom: '15px' }}> { message.message } </p>
                                    </div>
                                )
                            }
                        })
                    : ''}                    
                </ScrollToBottom>
                <br />
                <div style={{ margin: 'auto 220px' }}>
                    <ImpulseSpinner
                        size={30}
                        color="black"
                        loading={ loading }                        
                    />
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ width: '100%' }}>
                        <TextField
                            id="standard-name"
                            label="Escrever..."
                            margin="normal"
                            multiline
                            fullWidth
                            helperText={ loading ? "Enviando mensagem" : "Pressione Enter para enviar a mensagem" }
                            value={this.state.newMessage}
                            onChange={this.handleChange}
                            onKeyDown={this.onKeyDown}
                        />
                    </div>
                    <div>
                        <IconButton className={ classes.margin } disabled={ loading } style={{ marginTop: '20px' }} color="secondary" onClick={this.sendMenssage}>
                            <FontAwesomeIcon style={{ color:'#6db65b' }} className='fa-sm' icon={faPaperPlane} />
                        </IconButton>
                    </div>
                </div>
            </div>
        );
    }
}
Chat.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user_info
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, {})
)(Chat);