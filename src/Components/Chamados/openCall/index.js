import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';
import io from 'socket.io-client'
import api from '../../../api/api';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faTrash, faWindowClose, faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';
import Rating from 'material-ui-rating'

import Chat from './chat/index';

import { SwishSpinner } from 'react-spinners-kit';

const styles = theme => ({
    margin: {
        margin: theme.spacing.unit,
    },

    dialog: {
        width: '600px',
        height: '50%'
    },

    dialogTitle: {
        backgroundColor: '#81c784',
        height: '150px',
        padding: '0px 8px 30px',

        '& h2':{
            color: 'white',
            justifyContent: 'space-between',
            display: 'flex'            
        }
    },

    chat: {
        border: '1px solid #81c784',
        padding: '20px',
        borderRadius: '10px'
    },

    dialogRating: {
        width: '400px'
    },

    dialogTitleRating: {
        padding: '0px 8px 5px',
        backgroundColor: '#81c784',
        height: '121px',

        '& h2':{
            color: 'white',
            justifyContent: 'space-between',
            display: 'flex'            
        }
    },

    dialogContentRating: {
        margin: 'auto 115px',
        padding: '30px 0px'
    },
    btnConfirmRating: {
        color: 'white'
    }
})

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class DialogVerMais extends Component {

    state = {
        dados: [],
        open: false,
        openRating: false,
        loading: false,
        rating: 0,
        image: []
    }

    componentDidMount() {
        const socket = io(process.env.REACT_APP_API_URL)
        const { id, mensagens } = this.props;

        socket.on('ItemUpdate', data => {
            this.setState({
                loading: true
            })
            api.get('/chamado/' + id)
            .then(res => {
                this.setState({
                    dados: res.data,
                    loading: false
                })
            })          
        })
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    }

    handleClose = () => {
        this.setState({ open: false });
    }

    handleVerMais = (id) =>{
        this.setState({ loading: true })

        api.get('/chamado/'+ id)
        .then(res => {
            this.setState({
                dados: res.data,
                open: true
            })
        })

        api.get('/upload/' + id)            
            .then(res => this.setState({
                image: res.data,
                loading: false
            }))
    }

    onChangeRating = async (value) => {
        const { rating } = this.state;
        
        this.setState({
            rating: value
        })
    }

    ratingConfirm = async () => {
        const { id } = this.props;
        const { rating } = this.state

        api.put('/' + id, {
            rating: rating
        })
        .then(res => this.setState({ open: false }))
    }

    render(){
        const { classes, id, user } = this.props;
        const { dados, open, loading, rating, image } = this.state;

        let imageUrl = image.map((imageTicket) => imageTicket.url)

        return(
            <div>
                <IconButton className={ classes.margin } color="secondary" onClick={this.handleVerMais.bind(this, id)}>
                    <FontAwesomeIcon style={{ color:'#6db65b' }} className='fa-sm' icon={faPlusCircle} />
                </IconButton>
               
                <Dialog
                    open={open}
                    TransitionComponent={Transition}
                    maxWidth={ imageUrl ? '1200' : '600' }
                    keepMounted
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"                    
                >   
                    <div style={{ display: 'flex' }}>
                        { imageUrl.length > 0 ? (
                            <div  style={{ 
                                backgroundImage: 'url('+ imageUrl +')',
                                width: '600px', 
                                height: 'auto', 
                                backgroundImage: 'url('+ imageUrl +')', 
                                backgroundRepeat: 'no-repeat', 
                                backgroundSize: 'cover'
                            }}>
                                <SwishSpinner 
                                    size={30}
                                    color="primary"
                                    style={{ margin: '50%' }}
                                    loading={loading}
                                />
                            </div>
                        ): '' }
                        
                        
                        <div className={ classes.dialog }>
                            <DialogTitle id="alert-dialog-slide-title" className={ classes.dialogTitle }>
                                <div style={{ margin: '0px 20px' }}>
                                    <p>created by {dados.company ? dados.company : 'Undefined :('}</p>
                                    <h1>
                                        { dados.title }
                                        <span style={{ margin: '0px 10px', fontSize: '15px', color: 'teal' }}>{ dados.status } - { dados.priority }</span>
                                    </h1>
                                    <h3 style={{ color: 'teal', display: 'flex' }}>                                        
                                        { dados.rating ? (
                                            <div style={{ display: 'flex', margin: 'auto' }}>
                                                <FontAwesomeIcon style={{ color: 'yellow', margin: '0px 5px 0px 30px' }} className='fa-sm' icon={faStarHalfAlt} />
                                                { dados.rating }
                                            </div>                                    
                                        ):''}
                                    </h3>
                                </div>
                                <div>  
                                    <IconButton className={ classes.margin } onClick={this.handleClose}>
                                        <FontAwesomeIcon style={{ color:'white' }} className='fa-sm' icon={faWindowClose} />
                                    </IconButton>
                                </div>
                            </DialogTitle>
                            <br />
                            <DialogContent>
                                <DialogContentText id="alert-dialog-slide-description">
                                    {dados.description}
                                </DialogContentText>
                                <br />
                                <div className={ classes.chat }>
                                    <Chat mensagens={ dados.messages } loading={ loading } id={id}/>
                                </div>
                            </DialogContent>
                        </div>
                    </div>
                </Dialog>


                { dados.status == 'Done' && !dados.rating && user.userType == 'cliente' ? (
                    <Dialog
                        open={open}
                        TransitionComponent={Transition}
                        keepMounted
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-slide-title"
                        aria-describedby="alert-dialog-slide-description"                    
                    >   
                        <div className={ classes.dialogRating }>
                            <DialogTitle id="alert-dialog-slide-title" className={ classes.dialogTitleRating }>
                                <div >
                                    <h2>Dê uma nota para o atendimento recebido</h2>
                                </div>
                            </DialogTitle>
                            <br />
                            <DialogContent className={ classes.dialogContentRating }>
                                <div>
                                    <Rating
                                        value={rating}
                                        max={5}
                                        onChange={(value) => this.onChangeRating(value)}
                                    />
                                </div>
                            </DialogContent>
                            <DialogActions>
                                <Button variant="contained" size="small" className={ classes.btnConfirmRating } onClick={this.ratingConfirm} color="primary" autoFocus>
                                    Confirmar nota
                                </Button>
                            </DialogActions>
                        </div>
                    </Dialog>
                ): ''}
            </div>
        );
    }
}

const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user: user_info.user
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, {})
)(DialogVerMais);