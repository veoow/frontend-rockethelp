import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import api from '../../api/api';
import io from 'socket.io-client';

import moment from 'moment';

import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

const styles = theme => ({})

class ClientsComp extends Component {
    state = {
        columns: [
            'Nome', 'Telefone', 'Endereço', 'Pacote'
        ],

        dataReceive: [],

        options: { 
            filterType: "dropdown",
            responsive: "scroll",
            selectableRows: 'none',
            textLabels: {
                body: {
                  noMatch: "Sem dados a exibir!",
                  toolTip: "Sort",
                }
            }
        }
    }

    getMuiTheme = () => createMuiTheme({
        overrides: {
          MUIDataTableBodyCell: {
            root: {
              color: "green"
            }
          }
        }
      })

    componentDidMount() {
        const socket = io('http://localhost:3001/')
        const { dataReceive } = this.state;

        api.get('/clients')
            .then(res => this.setState({
                dataReceive: res.data
            }))

        socket.on('NewClient', data => {
            api.get('/clients')
            .then(res => this.setState({
                dataReceive: res.data
            }))
        });
    }

    render(){
        const { classes } = this.props;
        const { columns, dataReceive, options } = this.state;

        return(
            <div>
                <MuiThemeProvider theme={this.getMuiTheme()}>
                    <MUIDataTable
                        data={dataReceive.map(item => {               
                            return(
                                [
                                    item.company ? item.company : 'Veoow',
                                    item.telefone ? item.telefone : '',
                                    item.endereco ? item.endereco : '',
                                    item.pacote ? item.pacote : ''
                                ]
                            )
                        })}                     
                        columns={columns}
                        options={options}
                    />
                </MuiThemeProvider>
            </div>
        );
    }
}

ClientsComp.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles, { withTheme: true })(ClientsComp);