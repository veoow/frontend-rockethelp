import { createCanBoundTo } from "@casl/react";
import { ability } from "../../permissoes/ability";

export default createCanBoundTo(ability);