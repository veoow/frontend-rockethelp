import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';
import io from 'socket.io-client';
import api from '../../api/api';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { Pie, Line } from 'react-chartjs-2';
import { Divider, DialogTitle } from '@material-ui/core';

const styles = theme => ({})

class Dash extends Component {
    state = {
        data: {
            datasets: [{
                data: [0, 0, 0],
                backgroundColor: ["#F7464A", "yellow", "#6db65b"]
            }],
        
            labels: [
                'ToDO',
                'InProgress',
                'Done'
            ]
        },

        priority: [],

        lineData: {
            labels: ['Nota: 1', 'Nota: 2', 'Nota: 3', 'Nota: 4', 'Nota: 5'],
            datasets: [{
                label: 'Nota',
                backgroundColor: 'rgba(109, 182, 91, 0.5)', 
                data: [0, 0, 0, 0, 0] ,
            }]
        },
    }

    UNSAFE_componentWillMount() {
        const socket = io(process.env.REACT_APP_API_URL);
        const { data } = this.state;
        const { user } = this.props;

        api.get('/dashboard',{
            userType: user.userType,
            companyID: user.companyID,
            clientID: user._id,
            username: user.username
        }).then(res => {
            let status = res.data.status;

            this.setState({
                data: {
                    datasets: [{
                        data: [ status.todo, status.inprogress, status.done ]
                    }]
                },
                priority: res.data.priority
            })
        });

        socket.on('NewCall', dataNew => {
            api.get('/dashboard',{
                userType: user.userType,
                companyID: user.companyID,
                clientID: user._id
            }).then(res => {
                let status = res.data.status;
                let newData =  [ status.todo, status.inprogress, status.done ];

                this.setState({
                    data: {
                        datasets: {
                            data: newData
                        }
                    },
                    priority: res.data.priority
                })
            })
        });
    }

    setGradientColor = (canvas, color) => {
        const ctx = canvas.getContext('2d');
        const gradient = ctx.createLinearGradient(0,0, 600, 550);
    
        gradient.addColorStop(0, color);
        gradient.addColorStop(0.95, 'rgba(124, 144, 122, 0.5)');
    
        return gradient;
    }
    
    getChartData = canvas => {
        const { lineData } = this.state;
    
        if(lineData.datasets){
          let colors = [ 'rgba(109, 182, 91, 0.5)', 'rgba(0, 255, 0, 0.7)' ];
          lineData.datasets.forEach((set, i) => {
            set.backgroundColor = this.setGradientColor(canvas, colors[i]);
            set.borderColor = "white";
            set.borderWidth = 2;
          });
        }
    
        return lineData;
    }

    render(){
        const { classes } = this.props;
        const { data, priority } = this.state;

        return(
            <div style={{ display: 'flex' }}>
                <div style={{ width: '30%' }}>
                    <Paper style={{ padding: '20px', color: '#6db65b' }}>
                        <Typography>Status Gráfico</Typography>
                        <Divider />
                        <Pie 
                            options={{
                                responsive: true
                            }}
                            data={data}
                        />
                    </Paper>
                </div>

                {/* <div style={{ width: '30%', marginLeft: '20px' }}>
                    <Paper style={{ padding: '20px', color: '#6db65b', height: '88%' }}>
                        <p>Satisfação Gráfico</p>
                        <Divider />
                        <Line 
                            options={{
                            responsive: true
                            }}
                            data={this.getChartData}
                        />
                    </Paper>
                </div> */}
                
                <Paper style={{ padding: '20px', marginLeft: '20px' }}>
                    <Typography>Info Gráfico</Typography>
                    <Divider />
                    <br/>
                    <br/>
                    <Typography style={{ color: 'white', padding: '15px 10px', backgroundColor: '#6db65b' }}>Chamados Easy Priority:  { priority.easy }</Typography>
                    <Typography style={{ color: 'white', padding: '15px 10px', backgroundColor: '#fb8c00' }}>Chamados Medium Priority:  { priority.medium }</Typography>  
                    <Typography style={{ color: 'white', padding: '15px 10px', backgroundColor: '#e53935' }}>Chamados Hard Priority:  { priority.hard }</Typography>           
                </Paper>
            </div>
        );
    }
}

Dash.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user: user_info.user
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, {})
)(Dash);