import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { compose } from 'compose-react';
import io from 'socket.io-client';
import api from '../../api/api';
import moment from 'moment';

import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import Badge from '@material-ui/core/Badge';
import Popover from '@material-ui/core/Popover';
import { Divider } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faCommentAlt, faTicketAlt } from '@fortawesome/free-solid-svg-icons';

const styles = theme => ({
    margin: {
        margin: 0,
    },

    chip: {
        color: 'white'
    },

    card: {
        maxWidth: '100%',
        minWidth: '100%',
        margin: '0px 10px 10px 0px'
    },

    emptyUser: {
        padding: '10px',
        margin: 'auto 39%',
        textAlign: 'center',
        backgroundColor: 'ghostwhite',
        color: '#6db65b'
    },

    scroll: {
        height: 300,
    
        '& ::-webkit-scrollbar-track': {
            backgroundColor: '#F4F4F4',
            borderRadius: '10px',
        },
        '& ::-webkit-scrollbar': {
            width: '6px',
            borderRadius: '10px',
            background: '#F4F4F4'
        },
        '& ::-webkit-scrollbar-thumb': {
            background: '#dad7d7',
            borderRadius: '10px',
        }
    },
})

class UsuariosComponent extends Component {
    state = {
        data: [
            { message: 'New Message', type: 'newMessage', createdAt: 'Julho - 10, 2019', company: 'Teste' },
            { message: 'You Have a New Ticket', type: 'newTicket', createdAt: 'Julho - 10, 2019', company: 'Teste2' },
            { message: 'New Message', type: 'newMessage', createdAt: 'Julho - 10, 2019', company: 'Teste' },
            { message: 'You Have a New Ticket', type: 'newTicket', createdAt: 'Julho - 10, 2019', company: 'Teste2' },
            { message: 'New Message', type: 'newMessage', createdAt: 'Julho - 10, 2019', company: 'Teste' },
            { message: 'You Have a New Ticket', type: 'newTicket', createdAt: 'Julho - 10, 2019', company: 'Teste2' },
            { message: 'New Message', type: 'newMessage', createdAt: 'Julho - 10, 2019', company: 'Teste' },
            { message: 'You Have a New Ticket', type: 'newTicket', createdAt: 'Julho - 10, 2019', company: 'Teste2' }
        ],
        loading: false,
        open: false,
        anchorEl: null,
        invisible: false
    }

    componentDidMount() {
        const socket = io(process.env.REACT_APP_API_URL)
        const { user } = this.props;     
    }

    handleOpen = (event) => {
        this.setState({ anchorEl: event.currentTarget, open: true })
    }

    handleClose = () => {
        this.setState({ anchorEl: null, open: false })
    }

    render(){
        const { classes, user } = this.props;
        const { loading, data, invisible, open, anchorEl } = this.state;
        
        return(
            <div>
                <IconButton onClick={this.handleOpen}>
                    <Badge color="error" variant="dot" invisible={invisible} color="secondary" className={classes.margin}>
                        <FontAwesomeIcon style={{color:"white"}} className='fa-sm' icon={faBell} />
                    </Badge>
                </IconButton>

                <Popover
                    open={open}
                    anchorEl={anchorEl}
                    onClose={this.handleClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}                  
                >
                    <div style={{ minHeight: '200px', width: '400px', padding: '0px 20px' }}>
                        { loading ? (<CircularProgress style={{ margin: '55% 38%' }} color="secondary" />) : ''}
                        {
                            data.length > 0 ?
                                <MenuList className={ classes.scroll}>{
                                    data.map((item) => (
                                        <div style={{ margin: '15px 0px' }}>
                                            <MenuItem onClick={() => {}} style={{ paddingLeft: '0px', paddingRight: '0px' }}>
                                                <Avatar style={{ marginRight: '10px', width: '32px', height: '32px' }}><FontAwesomeIcon style={{color:"white"}} className='fa-sm' 
                                                    icon={
                                                        item.type == 'newMessage' ?
                                                            faCommentAlt
                                                        : item.type == 'newTicket' ?
                                                            faTicketAlt : ''
                                                    } 
                                                /></Avatar>
                                                <p style={{ margin: '0px 10px' }}>
                                                    {item.message} by {item.company}                                                
                                                </p>                                            
                                            </MenuItem>
                                            <Divider />
                                        </div>
                                    ))
                                }</MenuList>
                            :(
                               <p>Tudo ok!</p>
                            )
                        }
                        
                    </div>
                </Popover>
            </div>
        );
    }
}

UsuariosComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ usuario }) => {
    const { user_info } = usuario;
    
    return {
        user: user_info.user
    }
}

export default compose(
    withStyles(styles, { withTheme: true }),
    connect(mapStateToProps, {})
)(UsuariosComponent);