import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

//Actions
import { Creators as ActionsUser } from '../store/ducks/Usuario';

//theme
import theme from '../theme/default';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import Login from '../pages/Login/index';
import AppRoutes from './routes';

import api from '../api/api';

const PrivateRoute = ({ component: Component, auth, ...rest }) => {
    return(
        <Route {...rest} render={(props) => 
            auth ?(
                <Component {...props} />
            ):(
                <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            )}
        />
    )
};

const LoginRoute = ({ component: Component, auth, ...rest }) => {
    return(
        <Route {...rest} render={(props) => 
            auth ? (
                <Redirect to={{ pathname: '/app/dashboard', state: { from: props.location } }} />
            ):(
                <Component {...props} />
            )}
        />
    )
};

class AppRoute extends React.Component {

    state = {
        valid: false
    }

    UNSAFE_componentWillMount() {
        const { user, SetLogin } = this.props;        
         api.post('/token').then( res => {
            if(res.ok){
                SetLogado(true)
            }else{
                SetLogado(false)
            }
        })
    }

    render(){
        const { user, logado, location } = this.props;
        if(location.pathname === '/'){
            if(logado){
                return <Redirect to='/app/dashboard'/>
            }else{
                return <Redirect to='/login'/>
            }
        }

        return(
            <MuiThemeProvider theme={theme}>
                <div className="app-main">
                    <BrowserRouter>
                        <Switch>
                            <PrivateRoute path="/app" auth={user} component={AppRoutes}/>
                            <LoginRoute path="/login" auth={user} component={Login}/>
                        </Switch>
                    </BrowserRouter>
                </div>
            </MuiThemeProvider>
        );
    }
}

const mapStateToProps = ({ usuario }) => {
    const { logado } = usuario;

    return {
      user: usuario.user_info.user,
      logado 
    }
}

const { SetLogado } = ActionsUser;

export default connect(mapStateToProps, {
    SetLogado
})(AppRoute)