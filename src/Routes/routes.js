import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'compose-react';

import Main from '../pages/Main/index';
import Chamados from '../pages/Chamados/index';
import Clients from '../pages/Clients/index';
import Login from '../pages/Login/index';
import Usuarios from '../pages/Usuarios/index';

import Can from '../Components/Can/can';
import { atualizarPermissoes } from '../permissoes/ability';

class Routes extends Component{
  render(){
    const { user } = this.props
    return(
      <div>
        <BrowserRouter>
          { atualizarPermissoes(user) }  
          
          <Can do="read" on={{ __type: 'tickets' }}>
            <Route path="/app/dashboard" component={Main}/>
          </Can>

          <Can do="read" on={{ __type: 'tickets' }}>
            <Route path="/app/tickets" component={Chamados} />
          </Can>

          <Can do="read" on={{ __type: 'clientes' }}>
            <Route path="/app/clientes" component={Clients} />
          </Can>

          <Can do="read" on={{ __type: 'usuarios' }}>
            <Route path="/app/usuarios" component={Usuarios} />
          </Can>
        </BrowserRouter>
      </div>
    )
  }
};

const mapStateToProps = ({ usuario }) => {
  return {
    user: usuario.user_info.user
  }
}

export default connect(mapStateToProps, {})(Routes);