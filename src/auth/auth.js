import api from '../api/api';
import { connect } from 'react-redux';

//Ducks Actions
import { Creators as ActionsUser } from '../store/ducks/Usuario';

class Auth {
    constructor(){
        this.authenticated = false;
        this.user = [];
    }

    login = async (username, password, cb) => {
        let data = [];
        await api.post('/auth', {
            username: username, 
            password: password            
        }).then(async res => {
            data = res.data
            localStorage.clear();
            data.token ? localStorage.setItem('token', data.token)  : console.log('Token no provided')
            data.token ? this.authenticated = true : this.authenticated = false      
            this.user = data.user;
            localStorage.setItem('auth', this.authenticated)
        }) 
    }

    logout = async () => {
        localStorage.clear();
    }

    tokenVerify = (token) => {
        api.post('/token').then( res => {
            if(res.ok){
                return true;
            }else{
                return false;
            }
        })
    }
}

export default new Auth();